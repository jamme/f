# Multichannel IRC bot
Harrybotter is multi server IRC bot for running whois queries. Here are some instructions to get started.

## Requirements:
- Python 2 (version 2.6 >)

## Installation:
- Copy the threadbot.py to the folder you want to run it in
- Copy the config.ini file to the same folder with threadbot.by
- Edit the configuration file to your liking
- Run with python threadbot.by

## Editing configuration file:
The file consists of sections. Each section is denoted by [section]. Each section should have the following properties, Server, Port and Channel. Server and port are the parameters for connecting the IRC server and Channel is the name of the Command and Control Channel.

Example:
```
[testserver]
Server: irc.testserver.org
Port: 6667
Channel: #commandcontrol

[testserver2]
Server: irc.test.org
Port: 6667
Channel: #candc
```

## Usage:
Join the command and control channel in the server of your liking where Harrybotter has also joined. You can now start asking him questions

#### Query syntax:
The reason for this syntax is to give the possibility to run a command against a single server or all of them at the same time.

```
<command> <target> <arguments>
```

#### Valid commands are:
At the present only whois has been implemented
- whois

### Examples:
Whois query against a single server:

**whois <servername from the configuration file> <nick>**
```
whois testserver john
```

**Whois query against all of the servers in the configuration file:**
```
whois all john
```
The servername "all" is a special trigger to run the query against all of the server at the same time.

